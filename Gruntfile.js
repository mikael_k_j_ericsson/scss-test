module.exports = function (grunt) {
	var customerName = grunt.option('customer');


	grunt.initConfig({
		sass: {
			variant_00: {
				options: {
					sourcemap:	'none',
//					style: 		'compressed',
					loadPath: 	["00/customer/" + customerName],
				},
				files: {
					'00/build/common.css': ['00/scss/main.scss']
				}
			},
			variant_01: {
				options: {
					sourcemap:	'none',
//					style: 		'compressed',
					loadPath: 	["01/customer/" + customerName + '/scss'],
				},
				files: {
					'01/build/common.css': ['01/scss/main.scss']
				}
			},
			variant_02: {
				options: {
					sourcemap:	'none',
					style: 		'compressed',
					loadPath: 	['02/customer/' + customerName + '/scss']
				},
				files: {
					'02/build/common.css': '02/scss/main.scss'
				}
			}
		}
	});


	grunt.loadNpmTasks('grunt-contrib-sass');


	grunt.registerTask('build_00', ['sass:variant_00']);
	grunt.registerTask('build_01', ['sass:variant_01']);
	grunt.registerTask('build_02', ['sass:variant_02']);

	grunt.registerTask('build', ['build_00', 'build_01', 'build_02']);


	grunt.registerTask('extract_css_class_list', function (filePath) {
		var scssVariableList = {};
		var matchList = grunt.file.read(filePath, "utf8").match(/class\s*=\s*'.*?'/gm);

		for (var matchIndex in matchList) {
			var t = (matchList[matchIndex]).replace(/(class\s*=\s*'|')/gm, '').split(' ');
			for (i in t) {
				scssVariableList[t[i]] = true;
			}
		}

		console.log(Object.keys(scssVariableList).join('\n'));
	});


	grunt.registerTask('extract_scss_variable_list', function (filePath) {
		var scssVariableList = {};
		var matchList = grunt.file.read(filePath, "utf8").match(/\$[\-0-9_a-zA-Z]+/gm);

		for (var matchIndex in matchList) { var match = (matchList[matchIndex]);
			scssVariableList[match] = true;
		}

		console.log(Object.keys(scssVariableList).join('\n'));


	grunt.registerTask('parse_scss_variable_name', function (scssVaraiableName) {
		var fragmentList = scssVaraiableName.replace(/--/g, ' --').replace(/__/g, ' <>').split(' ');

		fragmentList = fragmentList.concat(fragmentList.pop().split('_'));

		var blockName 			= fragmentList.shift();
		var blockModifierName 	= null;
		var elementName 		= null;
		var elementModifierName = null;
		var detailName 			= null;
		var cssAttributeName 	= fragmentList.pop();

		while (fragmentList.length > 0) {
			var fragment = fragmentList.shift();

			if (fragment.indexOf("<>") == 0) { 
				elementName = fragment.substr(2); 	
			}
			else if (fragment.indexOf("--") == 0) {
				if (elementName == null) {
					blockModifierName = fragment.substr(2);
				}
				else {
					elementModifierName = fragment.substr(2);
				}
			}
			else {
				detailName = fragment;
			}
		}

		console.log("block:           " + blockName);		
		if (blockModifierName !== null) { 	console.log("blockModifier:   " + blockModifierName);	}
		if (elementName !== null) { 		console.log("element:         " + elementName);	}
		if (elementModifierName !== null) { console.log("elementModifier: " + elementModifierName);	}
		if (detailName !== null) { 			console.log("detail:          " + detailName);	}
		console.log("cssAttribute:    " + cssAttributeName);
	});
};
